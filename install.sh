#!/bin/bash

echo "Updating repo..."
apt update

echo "Installing apt packages..."
apt -y install python3-pip python3-pil libusb-dev libusb-1.0-0.dev

echo "Installing python packages..."
pip install adafruit-circuitpython-ssd1306