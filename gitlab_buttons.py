# Author: Cristiano Casella
#
# Based on examples found here: https://github.com/adafruit/Adafruit_SSD1306
# from
# Copyright (c) 2017 Adafruit Industries
# Author: James DeVito
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# This example is for use on (Linux) computers that are using CPython with
# Adafruit Blinka to support CircuitPython libraries. CircuitPython does
# not support PIL/pillow (python imaging library)!

import board
import busio
from digitalio import DigitalInOut, Direction, Pull
from PIL import Image, ImageDraw, ImageFont
import adafruit_ssd1306

# Create the I2C interface.
i2c = busio.I2C(board.SCL, board.SDA)
# Create the SSD1306 OLED class.
disp = adafruit_ssd1306.SSD1306_I2C(128, 64, i2c)

# Input pins:
button_A = DigitalInOut(board.D5)
button_A.direction = Direction.INPUT
button_A.pull = Pull.UP

button_B = DigitalInOut(board.D6)
button_B.direction = Direction.INPUT
button_B.pull = Pull.UP

button_L = DigitalInOut(board.D27)
button_L.direction = Direction.INPUT
button_L.pull = Pull.UP

button_R = DigitalInOut(board.D23)
button_R.direction = Direction.INPUT
button_R.pull = Pull.UP

button_U = DigitalInOut(board.D17)
button_U.direction = Direction.INPUT
button_U.pull = Pull.UP

button_D = DigitalInOut(board.D22)
button_D.direction = Direction.INPUT
button_D.pull = Pull.UP

button_C = DigitalInOut(board.D4)
button_C.direction = Direction.INPUT
button_C.pull = Pull.UP

# Clear display.
disp.fill(0)
disp.show()

# Create blank image for drawing.
# Make sure to create image with mode '1' for 1-bit color.
width = disp.width
height = disp.height
image = Image.new('1', (width, height))

# Get drawing object to draw on image.
draw = ImageDraw.Draw(image)

# Draw a black filled box to clear the image.
def clear():
    draw.rectangle((0, 0, width, height), outline=0, fill=0)

# Fonts settings
padding = -2
top = padding
bottom = height-padding
x = 0
#font = ImageFont.load_default()
font1 = ImageFont.truetype('/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf', 16)
font2 = ImageFont.truetype('/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf', 10)

# Texts and image functions
def title():
    clear()
    draw.text((x+0, top+0), "Cristiano", font=font1, fill=255)
    draw.text((x+0, top+17), "Casella", font=font1, fill=255)
    draw.text((x+0, top+34), "---------------", font=font1, fill=255)
    draw.text((x+0, top+51), "TAM EMEA", font=font2, fill=255)

def tanuki():
    clear()
    catImage = Image.open('gitlab.ppm').convert('1')
    disp.image(catImage)

def contact():
    clear()
    draw.text((x+0, top+30), "mailto:", font=font2, fill=255)
    draw.text((x+0, top+40), "ccasella@gitlab.com", font=font2, fill=255)

# Let's run
while True:
    # Show the title if you press A or B
    if not button_A.value or not button_B.value: 
        title()
    # Show the email if you press or move the joystick
    if not button_C.value or not button_U.value or not button_D.value or not button_L.value or not button_R.value:
        contact()

    # If nothing is pressed show tanuki logo
    if button_A.value and button_B.value and button_C.value and button_U.value and button_D.value and button_L.value and button_R.value:
        tanuki()
    else:
        disp.image(image)

    disp.show()
